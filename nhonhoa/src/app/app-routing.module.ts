import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessDeniedComponent } from './403/access-denied.component';
import { NoContentComponent } from './404/not-found.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/app/department/list',
    pathMatch: 'full'
  },

  // Login Module
  {
    path: 'app',
    loadChildren: () => import('./modules/layout/vertical/layout-1/layout-1.module').then(m => m.VerticalLayout1Module)
  },

  /* No Content */
  { path: 'access-denied', component: AccessDeniedComponent },
  { path: '**', component: NoContentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
