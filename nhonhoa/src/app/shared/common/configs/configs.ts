import { MatSnackBarConfig } from '@angular/material/snack-bar';
import * as moment from 'moment';


export module Configs {
  export const languageCodeKey = 'languageCode';
  export const StartYear = 2015;
  export const fullMonths = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  export const shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  export const PageIndex = 1;
  export const PageSize = 25;
  export const VAT = 10;
  export const FileMaximumSize = 5; // MB
  export const DivideItemNumber = 6;

  export const Regex = {
    Identity: '^[\\d]{9,15}$',
    PersonNaming:
      '^[a-zA-ZsÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀẾỂưăạảấầẩẫậắằẳẵặẹẻẽếềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$',
    PhoneNumber: '^[^-]*((0[1|2|3|4|5|6|7|8|9])+([0-9]{8,12}))\\b$'
  };



  export const TokenPrefix = 'supplychain';
  export const FileExtensions = ['docx', 'doc', 'pdf'];
  export const FileExtensionsContainer = [/*'xls',*/ 'xlsx'];
  export const ImageExtensions = ['png', 'jpg', 'gif', 'bmp', 'jpeg'];
  export const AllImageExtensions = 'image/*';
  export function NewGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (Math.random() * 16) | 0,
        v = c == 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }

  export function onGetEmptyObjectValues(inputObject: Object, isKeepObj: boolean = false): Object {
    var obj = {};
    if (inputObject != null) {
      Object.keys(inputObject).map(key => {
        obj[key] = isKeepObj ? obj[key] : '';
      });
    }
    return obj;
  }

  export const Genders = [{ value: 0, label: 'Không rõ' }, { value: 1, label: 'Nam' }, { value: 2, label: 'Nữ' }];

  export const PaymentSchedules = [
    { value: 0, label: 'Chưa xác định' },
    { value: 1, label: 'Các ngày trong tuần' },
    { value: 2, label: 'Ngày 15, 30 hàng tháng' },
    { value: 3, label: 'Ngày 10, 20, 30 hàng tháng' }
  ];

  export const DATE_FORMATS = {
    parse: {
      dateInput: 'DD/MM/YYYY'
    },
    display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'MM YYYY',
      dateA11yLabel: 'DD/MM/YYYY',
      monthYearA11yLabel: 'MM YYYY'
    }
  };

  export const MatSnackBarDefaultConfig: MatSnackBarConfig = {
    duration: 15 * 1000,
    verticalPosition: 'bottom',
    panelClass: 'snack-bar-container',
    horizontalPosition: 'right'
  };

  export function isValidDate(input: any, format: string = 'DD/MM/YYYY'): boolean {
    return input instanceof Date || moment(input, format).isValid();
  }

  export function GetDateFormatted(dateInput: Date | string, format: string = 'DD/MM/YYYY') {
    return dateInput ? moment(dateInput).format(format) : '';
  }

  export function ParseToDate(dateInput: Date | string, format: string = 'DD/MM/YYYY') {
    return dateInput ? moment(dateInput, format).format() : '';
  }

  export function cloneArrayOrObject(arrOrObj: Array<any> | Object): Array<any> | Object {
    return JSON.parse(JSON.stringify(arrOrObj));
  }

  const PREFIX: string = '';
  export const DECIMAL_SEPARATOR: string = '.';
  export const THOUSANDS_SEPARATOR: string = ',';
  export const BLANK: string = '';
  const SUFFIX: string = ''; //' $'
  const PADDING = '000000';

  export function TransformCurrency(value: string | number, fractionSize: number = 0): string {
    let [integer, fraction = ''] = (value || '').toString().split('.');

    fraction = fractionSize > 0 ? DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize) : '';

    integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, THOUSANDS_SEPARATOR);

    return PREFIX + integer + fraction + SUFFIX;
  }

  export function ParseCurrency(value: string, fractionSize: number = 2): string {
    let [integer, fraction = ''] = (value || '')
      .replace(this.PREFIX, '')
      .replace(this.SUFFIX, '')
      .split(this.DECIMAL_SEPARATOR);

    integer = integer.replace(new RegExp(this.THOUSANDS_SEPARATOR, 'g'), '');

    fraction =
      parseInt(fraction, 10) > 0 && fractionSize > 0
        ? this.DECIMAL_SEPARATOR + (fraction + PADDING).substring(0, fractionSize)
        : '';

    return integer + fraction;
  }

  export function validateFile(file: File, acceptFileExtensions: string[], maximumSize: number): string {
    if (!file) {
      return 'File không được để trống.';
    }

    if (file.size > maximumSize * 1024 * 1024) {
      return `Kích thước file không được vượt quá ${maximumSize} MB.`;
    }

    if (file.name !== null && file.name !== '' && file.name !== undefined) {
      let fileExt = file.name.replace(/^.*\./, '');
      if (acceptFileExtensions && acceptFileExtensions.length == 1 && acceptFileExtensions[0] == AllImageExtensions) {
        var pattern = /image-*/;
        if (!file.type.match(pattern)) {
          return `File cho phép tải lên: ${acceptFileExtensions.toString()}`;
        }
      } else if (
        acceptFileExtensions &&
        acceptFileExtensions.length > 0 &&
        !acceptFileExtensions.some(x => x.toUpperCase() == fileExt.toUpperCase())
      ) {
        return `File cho phép tải lên: ${acceptFileExtensions.toString()}`;
      }
    }

    return '';
  }

  export function BytesToSize(bytes: number): string {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0) return 'n/a';
    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)).toString(), 10);
    if (i === 0) return `${bytes} ${sizes[i]})`;
    return `${(bytes / 1024 ** i).toFixed(1)} ${sizes[i]}`;
  }
}
