import {
  HighlightTextDirective,
  EqualValidator,
  InputFocusDirective,
  NoWhitespaceDirective,
  DateComparison,
  TimeComparison,
  OnlyNumber,
  CalendarFocusDirective,
  DisplayContentDirective,
  TextAreaDirective,
  CurrencyFormatDirective,
  DecimalDirective,
  PercentDirective,
  PerfectScrollbarDirective
} from './directives';


export const SharedDirectives = [
  HighlightTextDirective,
  EqualValidator,
  InputFocusDirective,
  NoWhitespaceDirective,
  DateComparison,
  TimeComparison,
  OnlyNumber,
  CalendarFocusDirective,
  DisplayContentDirective,
  TextAreaDirective,
  CurrencyFormatDirective,
  DecimalDirective,
  PercentDirective,
  PerfectScrollbarDirective
];
