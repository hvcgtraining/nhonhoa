import { Directive, ElementRef, HostListener, Input, OnInit, DoCheck, EventEmitter, Output } from '@angular/core';
import { Configs } from 'src/app/shared/common/configs/configs';
import 'src/app/core/prototype/string-prototype';

@Directive({
  selector: '[currency-format],[currencyFormat]'
})
export class CurrencyFormatDirective implements OnInit, DoCheck {
  private el: any;
  private isDoChecked: boolean;
  @Output() valueChanged: EventEmitter<number> = new EventEmitter();
  @Input() acceptZero: boolean;

  constructor(private elementRef: ElementRef) {
    this.el = this.elementRef.nativeElement;
  }

  ngOnInit() {
    const outvalue = Number(this.el.value);
    this.el.value = Number.isNaN(outvalue) ? '' : Configs.TransformCurrency(this.el.value);
  }

  ngDoCheck(): void {
    if (this.el.value === '-') return;
    if (!!this.el.value /*&& !this.isDoChecked*/) {
      // const outvalue = Number(`${this.el.value}`.replaceAll(Configs.THOUSANDS_SEPARATOR, ''));
      // this.el.value = (Number.isNaN(outvalue) ? '' : Configs.TransformCurrency(outvalue));
      let outValue = this.el.value.replaceAll(Configs.THOUSANDS_SEPARATOR, Configs.BLANK);
      let numberData = Number(outValue);
      let i = 3;
      let pos = this.el.selectionStart;
      let index = (this.el.value.match(/,/g) || []).length;
      while (parseInt((Math.abs(numberData) / 1000).toString()) > 0) {
        let head = outValue.slice(0, outValue.length - i);
        let tail = outValue.slice(outValue.length - i, outValue.length);
        outValue = head + ',' + tail;
        numberData = parseInt((numberData / 1000).toString());
        if (index <= 0) {
          pos = pos + 1;
        }
        index--;
        i = i + 4;
      }
      this.el.value = outValue;
      this.el.setSelectionRange(pos, pos, 'none');
      this.isDoChecked = true;
    }
  }

  @HostListener('focus', ['$event.target.value'])
  onFocus(value: string) {
    // opossite of transform
    const outvalue = Number(value.replaceAll(Configs.THOUSANDS_SEPARATOR, ''));
    this.el.value = Number.isNaN(outvalue) ? '' : Configs.ParseCurrency(value);
  }

  private formatTimeout: any;
  @HostListener('blur', ['$event.target.value'])
  onBlur(value: string | number) {
    if (value == 0 && this.acceptZero == true) {
      return;
    }

    if (this.formatTimeout) {
      clearTimeout(this.formatTimeout);
    }
    this.formatTimeout = setTimeout(() => {
      const outvalue = Number(`${value}`.replaceAll(Configs.THOUSANDS_SEPARATOR, ''));
      this.el.value = Number.isNaN(outvalue) ? '' : Configs.TransformCurrency(outvalue);
      if (!Number.isNaN(outvalue)) this.valueChanged.emit(outvalue);
    }, 500);
  }
}
