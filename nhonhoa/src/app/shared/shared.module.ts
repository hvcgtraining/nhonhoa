import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';

import { SharedDirectives } from './share-directives';
import { SharedControls } from './share-controls';
import { SharedPipes } from './share-pipes';
import { GooglePlacesDirective } from './directives/google-places/google-places.directive';

import { ExcelUploadDialog, SnackBarComponent } from './controls';
import { LoadingComponent } from './components/loading/loading.component';
import { SharedComponents } from './share-components';



@NgModule({
  imports: [CommonModule, FormsModule, ReactiveFormsModule, MaterialModule, FlexLayoutModule, RouterModule],
  declarations: [SharedDirectives, SharedControls, SharedPipes,SnackBarComponent, SharedComponents, GooglePlacesDirective, ExcelUploadDialog],
  exports: [CommonModule, FormsModule, ReactiveFormsModule, SharedDirectives, SharedControls, SharedPipes, SharedComponents, ExcelUploadDialog, GooglePlacesDirective, SnackBarComponent, MaterialModule, FlexLayoutModule, RouterModule],
  providers: []
})
export class SharedModule {}
