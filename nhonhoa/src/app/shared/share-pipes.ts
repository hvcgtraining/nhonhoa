/**Pipes */
import {
  SafePipe,
  PayoffListStatusPipe,
  DayOfWeekVN,
  MonthNameVN,
  DayOfWeekNumberVN,
  PaymentStatusPipe
} from './pipes';

export const SharedPipes = [
  SafePipe,
  PayoffListStatusPipe,
  DayOfWeekVN,
  MonthNameVN,
  DayOfWeekNumberVN,
  PaymentStatusPipe
];
