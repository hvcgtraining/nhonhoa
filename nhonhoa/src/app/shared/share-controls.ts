/**Controls */
import {
  ConfirmDialogComponent,
  MessageDialogComponent,
  NotificationMessageComponent,
} from './controls';

export const SharedControls = [
  ConfirmDialogComponent,
  MessageDialogComponent,
  NotificationMessageComponent,
];
