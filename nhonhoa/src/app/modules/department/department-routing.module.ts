import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from 'src/app/core';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentAddComponent } from './department-add/department-add.component';
import { DepartmentUpdateComponent } from './department-update/department-update.component';
import { DepartmentViewComponent } from './department-view/department-view.component';



const routes: Routes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
    },
    {
        path: 'list',
        component: DepartmentListComponent,
        data: { title: extract('Components.Department.DepartmentList.Title') }
    },
    {
        path: 'add',
        component: DepartmentAddComponent,
        data: { title: extract('Components.Department.DepartmentAdd.Title') }
    },
    {
        path: 'update/:id',
        component: DepartmentUpdateComponent,
        data: { title: extract('Components.Department.DepartmentUpdate.Title') }
    },
    {
        path: 'view',
        component: DepartmentViewComponent,
        data: { title: extract('Components.Department.DepartmentView.Title') }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DepartmentRoutingModule { }
