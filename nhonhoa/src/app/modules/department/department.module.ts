import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentAddComponent } from './department-add/department-add.component';
import { DepartmentUpdateComponent } from './department-update/department-update.component';
import { DepartmentViewComponent } from './department-view/department-view.component';
import { DepartmentRoutingModule } from './department-routing.module';

@NgModule({
  declarations: [DepartmentListComponent, DepartmentAddComponent, DepartmentUpdateComponent, DepartmentViewComponent],
  imports: [
    SharedModule,
    DepartmentRoutingModule
  ]
})
export class DepartmentModule { }
