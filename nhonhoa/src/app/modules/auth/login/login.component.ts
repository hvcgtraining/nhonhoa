import { Component, OnInit } from '@angular/core';
import { UserContextModel } from 'src/app/shared/models/user/user.model';
import { FormBuilder, Validators } from '@angular/forms';
import { I18nService } from 'src/app/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public userLoginModel: UserContextModel = new UserContextModel();
  public loginForm: any;
  public isLoginError;
  public isLoading: boolean;

  constructor(
    private _fb: FormBuilder,
    private i18nService: I18nService
  ) { }

  ngOnInit() {
    // Set default language

    // Create Login Form
    this.loginForm = this._fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  onLogin() {
    if(this.loginForm.invalid) {
      return;
    }
    this.isLoading = true;
    this.disabledInput()
    setTimeout(()=> {
      this.isLoginError = true;
      this.isLoading= false;
      this.enabledInput();
    },2000)
  }

  disabledInput(): void {
    this.loginForm.get('username').disable();
    this.loginForm.get('password').disable();
  }

  enabledInput() {
    this.loginForm.get('username').enable();
    this.loginForm.get('password').enable();
  }
 
}
