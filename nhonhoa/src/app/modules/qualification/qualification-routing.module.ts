import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { extract } from 'src/app/core';
import { QualificationListComponent } from './qualification-list/qualification-list.component';
import { QualificationAddComponent } from './qualification-add/qualification-add.component';
import { QualificationUpdateComponent } from './qualification-update/qualification-update.component';
import { QualificationViewComponent } from './qualification-view/qualification-view.component';



const routes: Routes = [
    {
        path: '',
        redirectTo: '/app/qualification/list',
        pathMatch: 'full'
    },
    {
        path: 'list',
        component: QualificationListComponent,
        data: { title: extract('Components.Qualification.QualificationList.Title') }
    },
    {
        path: 'add',
        component: QualificationAddComponent,
        data: { title: extract('Components.Qualification.QualificationAdd.Title') }
    },
    {
        path: 'update/:id',
        component: QualificationUpdateComponent,
        data: { title: extract('Components.Qualification.QualificationUpdate.Title') }
    },
    {
        path: 'view',
        component: QualificationViewComponent,
        data: { title: extract('Components.Qualification.QualificationView.Title') }
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class QualificationRoutingModule { }
