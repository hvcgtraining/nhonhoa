import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { QualificationAddComponent } from './qualification-add/qualification-add.component';
import { QualificationListComponent } from './qualification-list/qualification-list.component';
import { QualificationUpdateComponent } from './qualification-update/qualification-update.component';
import { QualificationViewComponent } from './qualification-view/qualification-view.component';
import { QualificationRoutingModule } from './qualification-routing.module';

@NgModule({
  declarations: [QualificationAddComponent, QualificationListComponent, QualificationUpdateComponent, QualificationViewComponent],
  imports: [
    SharedModule,
    QualificationRoutingModule
  ]
})
export class QualificationModule { }
