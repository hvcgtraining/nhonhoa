import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerticalLayout1Component } from './layout-1.component';

const routes: Routes = [
    {
        path: '',
        component: VerticalLayout1Component,
        children: [
            {
                path: '',
                redirectTo: 'department',
                pathMatch: 'full'

            },
            {
                path: 'department',
                loadChildren: () => import('src/app/modules/department/department.module').then(m => m.DepartmentModule)
            },
            {
                path: 'qualification',
                loadChildren: () => import('src/app/modules/qualification/qualification.module').then(m => m.QualificationModule)
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class VerticalLayout1RoutingModule {

}