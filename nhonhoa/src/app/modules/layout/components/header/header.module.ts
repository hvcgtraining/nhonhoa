import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';

import { HeaderComponent } from './header.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { LanguageSelectorComponent } from './components/language-selector/language-selector.component';
import { HeaderNavComponent } from './components/header-nav/header-nav.component';


@NgModule({
    imports: [CommonModule, SharedModule, TranslateModule],
    declarations: [
        HeaderComponent,
        UserProfileComponent,
        LanguageSelectorComponent,
        HeaderNavComponent
    ],
    exports: [HeaderComponent, LanguageSelectorComponent]
})
export class HeaderModule { }