import { Component, OnInit, HostBinding, Input, ChangeDetectorRef } from "@angular/core";
import { NavItem } from "../nav.model";
import { NavigationService } from '../navigation.service';
import { Subject, merge } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: "nav-group",
  templateUrl: "./group.component.html",
  styleUrls: ["./group.component.scss"]
})
export class GroupComponent implements OnInit {
  @HostBinding("class")
  classes = "nav-group nav-item";

  @Input()
  item: NavItem;

   // Private
   private _unsubscribeAll: Subject<any>;

   /**
    * Constructor
    */

   /**
    *
    * @param {ChangeDetectorRef} _changeDetectorRef
    * @param {NavigationService} _navigationService
    */
   constructor(
       private _changeDetectorRef: ChangeDetectorRef,
       private _navigationService: NavigationService
   )
   {
       // Set the private defaults
       this._unsubscribeAll = new Subject();
   }

   // -----------------------------------------------------------------------------------------------------
   // @ Lifecycle hooks
   // -----------------------------------------------------------------------------------------------------

   /**
    * On init
    */
   ngOnInit(): void
   {
       // Subscribe to navigation item
       merge(
           this._navigationService.onNavigationItemAdded,
           this._navigationService.onNavigationItemUpdated,
           this._navigationService.onNavigationItemRemoved
       ).pipe(takeUntil(this._unsubscribeAll))
        .subscribe(() => {

            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
   }

   /**
    * On destroy
    */
   ngOnDestroy(): void
   {
       // Unsubscribe from all subscriptions
       this._unsubscribeAll.next();
       this._unsubscribeAll.complete();
   }

}
