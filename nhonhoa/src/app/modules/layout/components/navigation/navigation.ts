import { NavItem } from "./nav.model";

export const navigation: NavItem[] = [
  {
    id:"title",
    title: "Phòng Kế Hoạch",
    type: "group",
    children: [
      {
        id: "category",
        title: "Category",
        translateKey: "Components.Nav.SidebarNav.Category.Title",
        type: "collapsable",
        icon: "category",
        children: [
          {
            id: "department",
            title: "Department",
            translateKey: "Components.Nav.SidebarNav.Category.Children.Department",
            type: "item",
            routerLink: "/app/department",
          },
          {
            id: "team",
            title: "Team",
            translateKey: "Components.Nav.SidebarNav.Category.Children.TeamGroup",
            type: "item",
            routerLink: "/team",
          },
          {
            id: "worktime",
            title: "Work Time",
            translateKey: "Components.Nav.SidebarNav.Category.Children.WorkTime",
            type: "item",
            routerLink: "/worktime",
          },
          {
            id: "qualification",
            title: "Qualification",
            translateKey: "Components.Nav.SidebarNav.Category.Children.Qualification",
            type: "item",
            routerLink: "/app/qualification",
          }
        ]
      },
      {
        id: "team",
        title: "Team",
        translateKey: "Components.Nav.SidebarNav.Category.Children.TeamGroup",
        type: "item",
        icon: 'dashboard',
        routerLink: "/team",
      }
    ]
  }
  
]


