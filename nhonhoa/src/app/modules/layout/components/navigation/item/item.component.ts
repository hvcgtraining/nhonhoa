import { Component, OnInit, HostBinding, Input, ChangeDetectorRef } from "@angular/core";
import { NavItem } from "../nav.model";
import { NavigationService } from "../navigation.service";
import { takeUntil} from 'rxjs/operators';
import { Subject, merge  } from 'rxjs';
import { I18nService } from 'src/app/core';

@Component({
  selector: "nav-item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.scss"]
})
export class ItemComponent implements OnInit
{
  @HostBinding("class")
  classes = "nav-item";

  @Input()
  item: NavItem;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   */

  /**
   * @param {I18nService } _i18nService 
   * @param {ChangeDetectorRef} _changeDetectorRef
   * @param {NavigationService} _navigationService
   */
  constructor(
      private _changeDetectorRef: ChangeDetectorRef,
      private _navigationService: NavigationService,
      public _i18nService: I18nService 
  )
  {
      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void
  {
      // Subscribe to navigation item
      merge(
          this._navigationService.onNavigationItemAdded,
          this._navigationService.onNavigationItemUpdated,
          this._navigationService.onNavigationItemRemoved
      ).pipe(takeUntil(this._unsubscribeAll))
       .subscribe(() => {

           // Mark for check
           this._changeDetectorRef.markForCheck();
       });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void
  {
      // Unsubscribe from all subscriptions
      this._unsubscribeAll.next();
      this._unsubscribeAll.complete();
  }

  onClickItem($event) {
    // this._navigationService.onItemCollapsed.next(this.item);
  }
}
