import { Injectable } from '@angular/core';
import { StorageKey } from 'src/app/shared/models/storage-key/storage-key';
import { JwtHelperService } from '@auth0/angular-jwt';
import { StorageService } from 'src/app/shared/services/client/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private _redirectUrl: string;

  constructor(public jwtHelper: JwtHelperService, private storageService: StorageService) {}

  public isAuthenticated = (): boolean => {
    const token = this.storageService.onGetToken(StorageKey.Token);
    return !this.jwtHelper.isTokenExpired(token);
  };

  public set redirectUrl(value: string) {
    this._redirectUrl = value;
  }

  public get redirectUrl(): string {
    return this._redirectUrl;
  }
}
